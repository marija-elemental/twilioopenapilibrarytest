"""
    Twilio - Api

    This is the public Twilio REST API.  # noqa: E501

    The version of the OpenAPI document: 1.23.1
    Contact: support@twilio.com
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.api_v2010_account_conference import ApiV2010AccountConference
globals()['ApiV2010AccountConference'] = ApiV2010AccountConference
from openapi_client.model.list_conference_response import ListConferenceResponse


class TestListConferenceResponse(unittest.TestCase):
    """ListConferenceResponse unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testListConferenceResponse(self):
        """Test ListConferenceResponse"""
        # FIXME: construct object with mandatory attributes with example values
        # model = ListConferenceResponse()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

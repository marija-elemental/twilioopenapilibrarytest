"""
    Twilio - Api

    This is the public Twilio REST API.  # noqa: E501

    The version of the OpenAPI document: 1.23.1
    Contact: support@twilio.com
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.api_v2010_account_available_phone_number_country_available_phone_number_national import ApiV2010AccountAvailablePhoneNumberCountryAvailablePhoneNumberNational
globals()['ApiV2010AccountAvailablePhoneNumberCountryAvailablePhoneNumberNational'] = ApiV2010AccountAvailablePhoneNumberCountryAvailablePhoneNumberNational
from openapi_client.model.list_available_phone_number_national_response import ListAvailablePhoneNumberNationalResponse


class TestListAvailablePhoneNumberNationalResponse(unittest.TestCase):
    """ListAvailablePhoneNumberNationalResponse unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testListAvailablePhoneNumberNationalResponse(self):
        """Test ListAvailablePhoneNumberNationalResponse"""
        # FIXME: construct object with mandatory attributes with example values
        # model = ListAvailablePhoneNumberNationalResponse()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

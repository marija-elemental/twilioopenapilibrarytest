"""
    Twilio - Api

    This is the public Twilio REST API.  # noqa: E501

    The version of the OpenAPI document: 1.23.1
    Contact: support@twilio.com
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.api_v2010_account_sip_sip_credential_list import ApiV2010AccountSipSipCredentialList
globals()['ApiV2010AccountSipSipCredentialList'] = ApiV2010AccountSipSipCredentialList
from openapi_client.model.list_sip_credential_list_response import ListSipCredentialListResponse


class TestListSipCredentialListResponse(unittest.TestCase):
    """ListSipCredentialListResponse unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testListSipCredentialListResponse(self):
        """Test ListSipCredentialListResponse"""
        # FIXME: construct object with mandatory attributes with example values
        # model = ListSipCredentialListResponse()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

"""
    Twilio - Api

    This is the public Twilio REST API.  # noqa: E501

    The version of the OpenAPI document: 1.23.1
    Contact: support@twilio.com
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.api_v2010_account_available_phone_number_country_available_phone_number_local import ApiV2010AccountAvailablePhoneNumberCountryAvailablePhoneNumberLocal
globals()['ApiV2010AccountAvailablePhoneNumberCountryAvailablePhoneNumberLocal'] = ApiV2010AccountAvailablePhoneNumberCountryAvailablePhoneNumberLocal
from openapi_client.model.list_available_phone_number_local_response import ListAvailablePhoneNumberLocalResponse


class TestListAvailablePhoneNumberLocalResponse(unittest.TestCase):
    """ListAvailablePhoneNumberLocalResponse unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testListAvailablePhoneNumberLocalResponse(self):
        """Test ListAvailablePhoneNumberLocalResponse"""
        # FIXME: construct object with mandatory attributes with example values
        # model = ListAvailablePhoneNumberLocalResponse()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

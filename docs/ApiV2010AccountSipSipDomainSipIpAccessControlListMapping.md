# ApiV2010AccountSipSipDomainSipIpAccessControlListMapping


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The unique id of the Account that is responsible for this resource. | [optional] 
**date_created** | **str, none_type** | The date that this resource was created, given as GMT in RFC 2822 format. | [optional] 
**date_updated** | **str, none_type** | The date that this resource was last updated, given as GMT in RFC 2822 format. | [optional] 
**domain_sid** | **str, none_type** | The unique string that identifies the SipDomain resource. | [optional] 
**friendly_name** | **str, none_type** | A human readable descriptive text for this resource, up to 64 characters long. | [optional] 
**sid** | **str, none_type** | A 34 character string that uniquely identifies this resource. | [optional] 
**uri** | **str, none_type** | The URI for this resource, relative to https://api.twilio.com | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



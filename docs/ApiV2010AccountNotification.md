# ApiV2010AccountNotification


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created the resource | [optional] 
**api_version** | **str, none_type** | The API version used to generate the notification | [optional] 
**call_sid** | **str, none_type** | The SID of the Call the resource is associated with | [optional] 
**date_created** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was created | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was last updated | [optional] 
**error_code** | **str, none_type** | A unique error code corresponding to the notification | [optional] 
**log** | **str, none_type** | An integer log level | [optional] 
**message_date** | **str, none_type** | The date the notification was generated | [optional] 
**message_text** | **str, none_type** | The text of the notification | [optional] 
**more_info** | **str, none_type** | A URL for more information about the error code | [optional] 
**request_method** | **str, none_type** | HTTP method used with the request url | [optional] 
**request_url** | **str, none_type** | URL of the resource that generated the notification | [optional] 
**sid** | **str, none_type** | The unique string that identifies the resource | [optional] 
**uri** | **str, none_type** | The URI of the resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



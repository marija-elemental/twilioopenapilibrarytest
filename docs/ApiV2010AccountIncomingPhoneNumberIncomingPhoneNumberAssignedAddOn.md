# ApiV2010AccountIncomingPhoneNumberIncomingPhoneNumberAssignedAddOn


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created the resource | [optional] 
**configuration** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | A JSON string that represents the current configuration | [optional] 
**date_created** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was created | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was last updated | [optional] 
**description** | **str, none_type** | A short description of the Add-on functionality | [optional] 
**friendly_name** | **str, none_type** | The string that you assigned to describe the resource | [optional] 
**resource_sid** | **str, none_type** | The SID of the Phone Number that installed this Add-on | [optional] 
**sid** | **str, none_type** | The unique string that identifies the resource | [optional] 
**subresource_uris** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | A list of related resources identified by their relative URIs | [optional] 
**unique_name** | **str, none_type** | An application-defined string that uniquely identifies the resource | [optional] 
**uri** | **str, none_type** | The URI of the resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# ApiV2010AccountConference


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created this resource | [optional] 
**api_version** | **str, none_type** | The API version used to create this conference | [optional] 
**call_sid_ending_conference** | **str, none_type** | The call SID that caused the conference to end | [optional] 
**date_created** | **str, none_type** | The RFC 2822 date and time in GMT that this resource was created | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that this resource was last updated | [optional] 
**friendly_name** | **str, none_type** | A string that you assigned to describe this conference room | [optional] 
**reason_conference_ended** | **str, none_type** | The reason why a conference ended. | [optional] 
**region** | **str, none_type** | A string that represents the Twilio Region where the conference was mixed | [optional] 
**sid** | **str, none_type** | The unique string that identifies this resource | [optional] 
**status** | **str, none_type** | The status of this conference | [optional] 
**subresource_uris** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | A list of related resources identified by their relative URIs | [optional] 
**uri** | **str, none_type** | The URI of this resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# ApiV2010AccountAvailablePhoneNumberCountryAvailablePhoneNumberLocalCapabilities

Whether a phone number can receive calls or messages

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fax** | **bool** |  | [optional] 
**mms** | **bool** |  | [optional] 
**sms** | **bool** |  | [optional] 
**voice** | **bool** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# ApiV2010AccountSigningKey


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_created** | **str, none_type** |  | [optional] 
**date_updated** | **str, none_type** |  | [optional] 
**friendly_name** | **str, none_type** |  | [optional] 
**sid** | **str, none_type** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



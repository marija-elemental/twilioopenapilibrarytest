# ApiV2010AccountUsageUsageRecordUsageRecordDaily


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account accrued the usage | [optional] 
**api_version** | **str, none_type** | The API version used to create the resource | [optional] 
**as_of** | **str, none_type** | Usage records up to date as of this timestamp | [optional] 
**category** | **str, none_type** | The category of usage | [optional] 
**count** | **str, none_type** | The number of usage events | [optional] 
**count_unit** | **str, none_type** | The units in which count is measured | [optional] 
**description** | **str, none_type** | A plain-language description of the usage category | [optional] 
**end_date** | **date, none_type** | The last date for which usage is included in the UsageRecord | [optional] 
**price** | **float, none_type** | The total price of the usage | [optional] 
**price_unit** | **str, none_type** | The currency in which &#x60;price&#x60; is measured | [optional] 
**start_date** | **date, none_type** | The first date for which usage is included in this UsageRecord | [optional] 
**subresource_uris** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | A list of related resources identified by their relative URIs | [optional] 
**uri** | **str, none_type** | The URI of the resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**usage** | **str, none_type** | The amount of usage | [optional] 
**usage_unit** | **str, none_type** | The units in which usage is measured | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



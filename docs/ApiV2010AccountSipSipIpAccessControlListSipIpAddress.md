# ApiV2010AccountSipSipIpAccessControlListSipIpAddress


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The unique id of the Account that is responsible for this resource. | [optional] 
**cidr_prefix_length** | **int, none_type** | An integer representing the length of the CIDR prefix to use with this IP address when accepting traffic. By default the entire IP address is used. | [optional] 
**date_created** | **str, none_type** | The date that this resource was created, given as GMT in RFC 2822 format. | [optional] 
**date_updated** | **str, none_type** | The date that this resource was last updated, given as GMT in RFC 2822 format. | [optional] 
**friendly_name** | **str, none_type** | A human readable descriptive text for this resource, up to 64 characters long. | [optional] 
**ip_access_control_list_sid** | **str, none_type** | The unique id of the IpAccessControlList resource that includes this resource. | [optional] 
**ip_address** | **str, none_type** | An IP address in dotted decimal notation from which you want to accept traffic. Any SIP requests from this IP address will be allowed by Twilio. IPv4 only supported today. | [optional] 
**sid** | **str, none_type** | A 34 character string that uniquely identifies this resource. | [optional] 
**uri** | **str, none_type** | The URI for this resource, relative to https://api.twilio.com | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



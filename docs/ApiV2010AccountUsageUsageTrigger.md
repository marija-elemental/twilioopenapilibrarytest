# ApiV2010AccountUsageUsageTrigger


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that this trigger monitors | [optional] 
**api_version** | **str, none_type** | The API version used to create the resource | [optional] 
**callback_method** | **str, none_type** | The HTTP method we use to call callback_url | [optional] 
**callback_url** | **str, none_type** | he URL we call when the trigger fires | [optional] 
**current_value** | **str, none_type** | The current value of the field the trigger is watching | [optional] 
**date_created** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was created | [optional] 
**date_fired** | **str, none_type** | The RFC 2822 date and time in GMT that the trigger was last fired | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was last updated | [optional] 
**friendly_name** | **str, none_type** | The string that you assigned to describe the trigger | [optional] 
**recurring** | **str, none_type** | The frequency of a recurring UsageTrigger | [optional] 
**sid** | **str, none_type** | The unique string that identifies the resource | [optional] 
**trigger_by** | **str, none_type** | The field in the UsageRecord resource that fires the trigger | [optional] 
**trigger_value** | **str, none_type** | The value at which the trigger will fire | [optional] 
**uri** | **str, none_type** | The URI of the resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**usage_category** | **str, none_type** | The usage category the trigger watches | [optional] 
**usage_record_uri** | **str, none_type** | The URI of the UsageRecord resource this trigger watches | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



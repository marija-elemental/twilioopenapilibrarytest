# ApiV2010AccountCallCallEvent


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | Call Request. | [optional] 
**response** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | Call Response with Events. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



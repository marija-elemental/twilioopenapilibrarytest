# ApiV2010AccountShortCode


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created this resource | [optional] 
**api_version** | **str, none_type** | The API version used to start a new TwiML session | [optional] 
**date_created** | **str, none_type** | The RFC 2822 date and time in GMT that this resource was created | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that this resource was last updated | [optional] 
**friendly_name** | **str, none_type** | A string that you assigned to describe this resource | [optional] 
**short_code** | **str, none_type** | The short code. e.g., 894546. | [optional] 
**sid** | **str, none_type** | The unique string that identifies this resource | [optional] 
**sms_fallback_method** | **str, none_type** | HTTP method we use to call the sms_fallback_url | [optional] 
**sms_fallback_url** | **str, none_type** | URL Twilio will request if an error occurs in executing TwiML | [optional] 
**sms_method** | **str, none_type** | HTTP method to use when requesting the sms url | [optional] 
**sms_url** | **str, none_type** | URL we call when receiving an incoming SMS message to this short code | [optional] 
**uri** | **str, none_type** | The URI of this resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



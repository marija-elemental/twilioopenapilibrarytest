# ApiV2010AccountTranscription


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created the resource | [optional] 
**api_version** | **str, none_type** | The API version used to create the transcription | [optional] 
**date_created** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was created | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was last updated | [optional] 
**duration** | **str, none_type** | The duration of the transcribed audio in seconds. | [optional] 
**price** | **float, none_type** | The charge for the transcription | [optional] 
**price_unit** | **str, none_type** | The currency in which price is measured | [optional] 
**recording_sid** | **str, none_type** | The SID that identifies the transcription&#39;s recording | [optional] 
**sid** | **str, none_type** | The unique string that identifies the resource | [optional] 
**status** | **str, none_type** | The status of the transcription | [optional] 
**transcription_text** | **str, none_type** | The text content of the transcription. | [optional] 
**type** | **str, none_type** | The transcription type | [optional] 
**uri** | **str, none_type** | The URI of the resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



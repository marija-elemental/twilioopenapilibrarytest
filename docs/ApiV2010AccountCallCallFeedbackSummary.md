# ApiV2010AccountCallCallFeedbackSummary


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The unique sid that identifies this account | [optional] 
**call_count** | **int, none_type** | The total number of calls | [optional] 
**call_feedback_count** | **int, none_type** | The total number of calls with a feedback entry | [optional] 
**date_created** | **str, none_type** | The date this resource was created | [optional] 
**date_updated** | **str, none_type** | The date this resource was last updated | [optional] 
**end_date** | **date, none_type** | The latest feedback entry date in the summary | [optional] 
**include_subaccounts** | **bool, none_type** | Whether the feedback summary includes subaccounts | [optional] 
**issues** | **[{str: (bool, date, datetime, dict, float, int, list, str, none_type)}], none_type** | Issues experienced during the call | [optional] 
**quality_score_average** | **float, none_type** | The average QualityScore of the feedback entries | [optional] 
**quality_score_median** | **float, none_type** | The median QualityScore of the feedback entries | [optional] 
**quality_score_standard_deviation** | **float, none_type** | The standard deviation of the quality scores | [optional] 
**sid** | **str, none_type** | A string that uniquely identifies this feedback entry | [optional] 
**start_date** | **date, none_type** | The earliest feedback entry date in the summary | [optional] 
**status** | **str, none_type** | The status of the feedback summary | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# ApiV2010AccountQueueMember


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**call_sid** | **str, none_type** | The SID of the Call the resource is associated with | [optional] 
**date_enqueued** | **str, none_type** | The date the member was enqueued | [optional] 
**position** | **int, none_type** | This member&#39;s current position in the queue. | [optional] 
**queue_sid** | **str, none_type** | The SID of the Queue the member is in | [optional] 
**uri** | **str, none_type** | The URI of the resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**wait_time** | **int, none_type** | The number of seconds the member has been in the queue. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



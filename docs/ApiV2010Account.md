# ApiV2010Account


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auth_token** | **str, none_type** | The authorization token for this account | [optional] 
**date_created** | **str, none_type** | The date this account was created | [optional] 
**date_updated** | **str, none_type** | The date this account was last updated | [optional] 
**friendly_name** | **str, none_type** | A human readable description of this account | [optional] 
**owner_account_sid** | **str, none_type** | The unique 34 character id representing the parent of this account | [optional] 
**sid** | **str, none_type** | A 34 character string that uniquely identifies this resource. | [optional] 
**status** | **str, none_type** | The status of this account | [optional] 
**subresource_uris** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | Account Instance Subresources | [optional] 
**type** | **str, none_type** | The type of this account | [optional] 
**uri** | **str, none_type** | The URI for this resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



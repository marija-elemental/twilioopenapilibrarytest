# ApiV2010AccountMessageMedia


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created this resource | [optional] 
**content_type** | **str, none_type** | The default mime-type of the media | [optional] 
**date_created** | **str, none_type** | The RFC 2822 date and time in GMT that this resource was created | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that this resource was last updated | [optional] 
**parent_sid** | **str, none_type** | The SID of the resource that created the media | [optional] 
**sid** | **str, none_type** | The unique string that identifies this resource | [optional] 
**uri** | **str, none_type** | The URI of this resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



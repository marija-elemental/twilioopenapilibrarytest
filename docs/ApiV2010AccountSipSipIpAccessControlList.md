# ApiV2010AccountSipSipIpAccessControlList


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The unique sid that identifies this account | [optional] 
**date_created** | **str, none_type** | The date this resource was created | [optional] 
**date_updated** | **str, none_type** | The date this resource was last updated | [optional] 
**friendly_name** | **str, none_type** | A human readable description of this resource | [optional] 
**sid** | **str, none_type** | A string that uniquely identifies this resource | [optional] 
**subresource_uris** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | The IP addresses associated with this resource. | [optional] 
**uri** | **str, none_type** | The URI for this resource | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



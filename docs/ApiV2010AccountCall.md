# ApiV2010AccountCall


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created this resource | [optional] 
**annotation** | **str, none_type** | The annotation provided for the call | [optional] 
**answered_by** | **str, none_type** | Either &#x60;human&#x60; or &#x60;machine&#x60; if this call was initiated with answering machine detection. Empty otherwise. | [optional] 
**api_version** | **str, none_type** | The API Version used to create the call | [optional] 
**caller_name** | **str, none_type** | The caller&#39;s name if this call was an incoming call to a phone number with caller ID Lookup enabled. Otherwise, empty. | [optional] 
**date_created** | **str, none_type** | The RFC 2822 date and time in GMT that this resource was created | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that this resource was last updated | [optional] 
**direction** | **str, none_type** | A string describing the direction of the call. &#x60;inbound&#x60; for inbound calls, &#x60;outbound-api&#x60; for calls initiated via the REST API or &#x60;outbound-dial&#x60; for calls initiated by a &#x60;Dial&#x60; verb. | [optional] 
**duration** | **str, none_type** | The length of the call in seconds. | [optional] 
**end_time** | **str, none_type** | The end time of the call. Null if the call did not complete successfully. | [optional] 
**forwarded_from** | **str, none_type** | The forwarding phone number if this call was an incoming call forwarded from another number (depends on carrier supporting forwarding). Otherwise, empty. | [optional] 
**_from** | **str, none_type** | The phone number, SIP address or Client identifier that made this call. Phone numbers are in E.164 format (e.g., +16175551212). SIP addresses are formatted as &#x60;name@company.com&#x60;. Client identifiers are formatted &#x60;client:name&#x60;. | [optional] 
**from_formatted** | **str, none_type** | The calling phone number, SIP address, or Client identifier formatted for display. | [optional] 
**group_sid** | **str, none_type** | The Group SID associated with this call. If no Group is associated with the call, the field is empty. | [optional] 
**parent_call_sid** | **str, none_type** | The SID that identifies the call that created this leg. | [optional] 
**phone_number_sid** | **str, none_type** | If the call was inbound, this is the SID of the IncomingPhoneNumber resource that received the call. If the call was outbound, it is the SID of the OutgoingCallerId resource from which the call was placed. | [optional] 
**price** | **str, none_type** | The charge for this call, in the currency associated with the account. Populated after the call is completed. May not be immediately available. | [optional] 
**price_unit** | **str, none_type** | The currency in which &#x60;Price&#x60; is measured. | [optional] 
**queue_time** | **str, none_type** | The wait time in milliseconds before the call is placed. | [optional] 
**sid** | **str, none_type** | The unique string that identifies this resource | [optional] 
**start_time** | **str, none_type** | The start time of the call. Null if the call has not yet been dialed. | [optional] 
**status** | **str, none_type** | The status of this call. | [optional] 
**subresource_uris** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | A list of related subresources identified by their relative URIs | [optional] 
**to** | **str, none_type** | The phone number, SIP address or Client identifier that received this call. Phone numbers are in E.164 format (e.g., +16175551212). SIP addresses are formatted as &#x60;name@company.com&#x60;. Client identifiers are formatted &#x60;client:name&#x60;. | [optional] 
**to_formatted** | **str, none_type** | The phone number, SIP address or Client identifier that received this call. Formatted for display. | [optional] 
**trunk_sid** | **str, none_type** | The (optional) unique identifier of the trunk resource that was used for this call. | [optional] 
**uri** | **str, none_type** | The URI of this resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



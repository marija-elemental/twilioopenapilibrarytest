# ApiV2010AccountRecordingRecordingAddOnResultRecordingAddOnResultPayload


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created the resource | [optional] 
**add_on_configuration_sid** | **str, none_type** | The SID of the Add-on configuration | [optional] 
**add_on_result_sid** | **str, none_type** | The SID of the AddOnResult to which the payload belongs | [optional] 
**add_on_sid** | **str, none_type** | The SID of the Add-on to which the result belongs | [optional] 
**content_type** | **str, none_type** | The MIME type of the payload | [optional] 
**date_created** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was created | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was last updated | [optional] 
**label** | **str, none_type** | The string that describes the payload | [optional] 
**reference_sid** | **str, none_type** | The SID of the recording to which the AddOnResult resource that contains the payload belongs | [optional] 
**sid** | **str, none_type** | The unique string that identifies the resource | [optional] 
**subresource_uris** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | A list of related resources identified by their relative URIs | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



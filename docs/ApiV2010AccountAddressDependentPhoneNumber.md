# ApiV2010AccountAddressDependentPhoneNumber


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created the resource | [optional] 
**address_requirements** | **str, none_type** | Whether the phone number requires an Address registered with Twilio | [optional] 
**api_version** | **str, none_type** | The API version used to start a new TwiML session | [optional] 
**capabilities** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | Indicate if a phone can receive calls or messages | [optional] 
**date_created** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was created | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was last updated | [optional] 
**emergency_address_sid** | **str, none_type** | The emergency address configuration to use for emergency calling | [optional] 
**emergency_status** | **str, none_type** | Whether the phone number is enabled for emergency calling | [optional] 
**friendly_name** | **str, none_type** | The string that you assigned to describe the resource | [optional] 
**phone_number** | **str, none_type** | The phone number in E.164 format | [optional] 
**sid** | **str, none_type** | The unique string that identifies the resource | [optional] 
**sms_application_sid** | **str, none_type** | The SID of the application that handles SMS messages sent to the phone number | [optional] 
**sms_fallback_method** | **str, none_type** | The HTTP method used with sms_fallback_url | [optional] 
**sms_fallback_url** | **str, none_type** | The URL that we call when an error occurs while retrieving or executing the TwiML | [optional] 
**sms_method** | **str, none_type** | The HTTP method to use with sms_url | [optional] 
**sms_url** | **str, none_type** | The URL we call when the phone number receives an incoming SMS message | [optional] 
**status_callback** | **str, none_type** | The URL to send status information to your application | [optional] 
**status_callback_method** | **str, none_type** | The HTTP method we use to call status_callback | [optional] 
**trunk_sid** | **str, none_type** | The SID of the Trunk that handles calls to the phone number | [optional] 
**uri** | **str, none_type** | The URI of the resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**voice_application_sid** | **str, none_type** | The SID of the application that handles calls to the phone number | [optional] 
**voice_caller_id_lookup** | **bool, none_type** | Whether to lookup the caller&#39;s name | [optional] 
**voice_fallback_method** | **str, none_type** | The HTTP method used with voice_fallback_url | [optional] 
**voice_fallback_url** | **str, none_type** | The URL we call when an error occurs in TwiML | [optional] 
**voice_method** | **str, none_type** | The HTTP method used with the voice_url | [optional] 
**voice_url** | **str, none_type** | The URL we call when the phone number receives a call | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# ApiV2010AccountRecording


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created the resource | [optional] 
**api_version** | **str, none_type** | The API version used during the recording. | [optional] 
**call_sid** | **str, none_type** | The SID of the Call the resource is associated with | [optional] 
**channels** | **int, none_type** | The number of channels in the final recording file as an integer. | [optional] 
**conference_sid** | **str, none_type** | The unique ID for the conference associated with the recording. | [optional] 
**date_created** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was created | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was last updated | [optional] 
**duration** | **str, none_type** | The length of the recording in seconds. | [optional] 
**encryption_details** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | How to decrypt the recording. | [optional] 
**error_code** | **int, none_type** | More information about why the recording is missing, if status is &#x60;absent&#x60;. | [optional] 
**price** | **str, none_type** | The one-time cost of creating the recording. | [optional] 
**price_unit** | **str, none_type** | The currency used in the price property. | [optional] 
**sid** | **str, none_type** | The unique string that identifies the resource | [optional] 
**source** | **str, none_type** | How the recording was created | [optional] 
**start_time** | **str, none_type** | The start time of the recording, given in RFC 2822 format | [optional] 
**status** | **str, none_type** | The status of the recording. | [optional] 
**subresource_uris** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | A list of related resources identified by their relative URIs | [optional] 
**uri** | **str, none_type** | The URI of the resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



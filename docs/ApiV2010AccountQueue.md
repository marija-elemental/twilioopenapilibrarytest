# ApiV2010AccountQueue


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created this resource | [optional] 
**average_wait_time** | **int, none_type** | Average wait time of members in the queue | [optional] 
**current_size** | **int, none_type** | The number of calls currently in the queue. | [optional] 
**date_created** | **str, none_type** | The RFC 2822 date and time in GMT that this resource was created | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that this resource was last updated | [optional] 
**friendly_name** | **str, none_type** | A string that you assigned to describe this resource | [optional] 
**max_size** | **int, none_type** | The max number of calls allowed in the queue | [optional] 
**sid** | **str, none_type** | The unique string that identifies this resource | [optional] 
**uri** | **str, none_type** | The URI of this resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



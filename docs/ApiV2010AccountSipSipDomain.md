# ApiV2010AccountSipSipDomain


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created the resource | [optional] 
**api_version** | **str, none_type** | The API version used to process the call | [optional] 
**auth_type** | **str, none_type** | The types of authentication mapped to the domain | [optional] 
**byoc_trunk_sid** | **str, none_type** | The SID of the BYOC Trunk resource. | [optional] 
**date_created** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was created | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was last updated | [optional] 
**domain_name** | **str, none_type** | The unique address on Twilio to route SIP traffic | [optional] 
**emergency_caller_sid** | **str, none_type** | Whether an emergency caller sid is configured for the domain. | [optional] 
**emergency_calling_enabled** | **bool, none_type** | Whether emergency calling is enabled for the domain. | [optional] 
**friendly_name** | **str, none_type** | The string that you assigned to describe the resource | [optional] 
**secure** | **bool, none_type** | Whether secure SIP is enabled for the domain | [optional] 
**sid** | **str, none_type** | The unique string that identifies the resource | [optional] 
**sip_registration** | **bool, none_type** | Whether SIP registration is allowed | [optional] 
**subresource_uris** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | A list mapping resources associated with the SIP Domain resource | [optional] 
**uri** | **str, none_type** | The URI of the resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**voice_fallback_method** | **str, none_type** | The HTTP method used with voice_fallback_url | [optional] 
**voice_fallback_url** | **str, none_type** | The URL we call when an error occurs while executing TwiML | [optional] 
**voice_method** | **str, none_type** | The HTTP method to use with voice_url | [optional] 
**voice_status_callback_method** | **str, none_type** | The HTTP method we use to call voice_status_callback_url | [optional] 
**voice_status_callback_url** | **str, none_type** | The URL that we call with status updates | [optional] 
**voice_url** | **str, none_type** | The URL we call when receiving a call | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



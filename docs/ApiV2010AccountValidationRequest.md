# ApiV2010AccountValidationRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created the resource | [optional] 
**call_sid** | **str, none_type** | The SID of the Call the resource is associated with | [optional] 
**friendly_name** | **str, none_type** | The string that you assigned to describe the resource | [optional] 
**phone_number** | **str, none_type** | The phone number to verify in E.164 format | [optional] 
**validation_code** | **str, none_type** | The 6 digit validation code that someone must enter to validate the Caller ID  when &#x60;phone_number&#x60; is called | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



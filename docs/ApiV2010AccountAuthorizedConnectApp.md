# ApiV2010AccountAuthorizedConnectApp


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created the resource | [optional] 
**connect_app_company_name** | **str, none_type** | The company name set for the Connect App | [optional] 
**connect_app_description** | **str, none_type** | A detailed description of the app | [optional] 
**connect_app_friendly_name** | **str, none_type** | The name of the Connect App | [optional] 
**connect_app_homepage_url** | **str, none_type** | The public URL for the Connect App | [optional] 
**connect_app_sid** | **str, none_type** | The SID that we assigned to the Connect App | [optional] 
**date_created** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was created | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was last updated | [optional] 
**permissions** | **[str], none_type** | Permissions authorized to the app | [optional] 
**uri** | **str, none_type** | The URI of the resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# ApiV2010AccountAddress


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that is responsible for the resource | [optional] 
**city** | **str, none_type** | The city in which the address is located | [optional] 
**customer_name** | **str, none_type** | The name associated with the address | [optional] 
**date_created** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was created | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was last updated | [optional] 
**emergency_enabled** | **bool, none_type** | Whether emergency calling has been enabled on this number | [optional] 
**friendly_name** | **str, none_type** | The string that you assigned to describe the resource | [optional] 
**iso_country** | **str, none_type** | The ISO country code of the address | [optional] 
**postal_code** | **str, none_type** | The postal code of the address | [optional] 
**region** | **str, none_type** | The state or region of the address | [optional] 
**sid** | **str, none_type** | The unique string that identifies the resource | [optional] 
**street** | **str, none_type** | The number and street address of the address | [optional] 
**uri** | **str, none_type** | The URI of the resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**validated** | **bool, none_type** | Whether the address has been validated to comply with local regulation | [optional] 
**verified** | **bool, none_type** | Whether the address has been verified to comply with regulation | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



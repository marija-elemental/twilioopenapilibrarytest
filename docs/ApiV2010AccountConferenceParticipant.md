# ApiV2010AccountConferenceParticipant


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created the resource | [optional] 
**call_sid** | **str, none_type** | The SID of the Call the resource is associated with | [optional] 
**call_sid_to_coach** | **str, none_type** | The SID of the participant who is being &#x60;coached&#x60; | [optional] 
**coaching** | **bool, none_type** | Indicates if the participant changed to coach | [optional] 
**conference_sid** | **str, none_type** | The SID of the conference the participant is in | [optional] 
**date_created** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was created | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was last updated | [optional] 
**end_conference_on_exit** | **bool, none_type** | Whether the conference ends when the participant leaves | [optional] 
**hold** | **bool, none_type** | Whether the participant is on hold | [optional] 
**label** | **str, none_type** | The label of this participant | [optional] 
**muted** | **bool, none_type** | Whether the participant is muted | [optional] 
**start_conference_on_enter** | **bool, none_type** | Whether the conference starts when the participant joins the conference | [optional] 
**status** | **str, none_type** | The status of the participant&#39;s call in a session | [optional] 
**uri** | **str, none_type** | The URI of the resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



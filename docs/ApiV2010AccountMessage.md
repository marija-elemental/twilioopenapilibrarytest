# ApiV2010AccountMessage


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created the resource | [optional] 
**api_version** | **str, none_type** | The API version used to process the message | [optional] 
**body** | **str, none_type** | The message text | [optional] 
**date_created** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was created | [optional] 
**date_sent** | **str, none_type** | The RFC 2822 date and time in GMT when the message was sent | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was last updated | [optional] 
**direction** | **str, none_type** | The direction of the message | [optional] 
**error_code** | **int, none_type** | The error code associated with the message | [optional] 
**error_message** | **str, none_type** | The description of the error_code | [optional] 
**_from** | **str, none_type** | The phone number that initiated the message | [optional] 
**messaging_service_sid** | **str, none_type** | The SID of the Messaging Service used with the message. | [optional] 
**num_media** | **str, none_type** | The number of media files associated with the message | [optional] 
**num_segments** | **str, none_type** | The number of messages used to deliver the message body | [optional] 
**price** | **str, none_type** | The amount billed for the message | [optional] 
**price_unit** | **str, none_type** | The currency in which price is measured | [optional] 
**sid** | **str, none_type** | The unique string that identifies the resource | [optional] 
**status** | **str, none_type** | The status of the message | [optional] 
**subresource_uris** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | A list of related resources identified by their relative URIs | [optional] 
**to** | **str, none_type** | The phone number that received the message | [optional] 
**uri** | **str, none_type** | The URI of the resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



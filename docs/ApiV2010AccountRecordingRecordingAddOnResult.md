# ApiV2010AccountRecordingRecordingAddOnResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created the resource | [optional] 
**add_on_configuration_sid** | **str, none_type** | The SID of the Add-on configuration | [optional] 
**add_on_sid** | **str, none_type** | The SID of the Add-on to which the result belongs | [optional] 
**date_completed** | **str, none_type** | The date and time in GMT that the result was completed | [optional] 
**date_created** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was created | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was last updated | [optional] 
**reference_sid** | **str, none_type** | The SID of the recording to which the AddOnResult resource belongs | [optional] 
**sid** | **str, none_type** | The unique string that identifies the resource | [optional] 
**status** | **str, none_type** | The status of the result | [optional] 
**subresource_uris** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | A list of related resources identified by their relative URIs | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



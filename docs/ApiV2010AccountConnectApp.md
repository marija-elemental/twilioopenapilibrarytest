# ApiV2010AccountConnectApp


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created the resource | [optional] 
**authorize_redirect_url** | **str, none_type** | The URL to redirect the user to after authorization | [optional] 
**company_name** | **str, none_type** | The company name set for the Connect App | [optional] 
**deauthorize_callback_method** | **str, none_type** | The HTTP method we use to call deauthorize_callback_url | [optional] 
**deauthorize_callback_url** | **str, none_type** | The URL we call to de-authorize the Connect App | [optional] 
**description** | **str, none_type** | The description of the Connect App | [optional] 
**friendly_name** | **str, none_type** | The string that you assigned to describe the resource | [optional] 
**homepage_url** | **str, none_type** | The URL users can obtain more information | [optional] 
**permissions** | **[str], none_type** | The set of permissions that your ConnectApp requests | [optional] 
**sid** | **str, none_type** | The unique string that identifies the resource | [optional] 
**uri** | **str, none_type** | The URI of the resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



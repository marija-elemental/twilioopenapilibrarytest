# ApiV2010AccountAvailablePhoneNumberCountry


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**beta** | **bool, none_type** | Whether all phone numbers available in the country are new to the Twilio platform. | [optional] 
**country** | **str, none_type** | The name of the country | [optional] 
**country_code** | **str, none_type** | The ISO-3166-1 country code of the country. | [optional] 
**subresource_uris** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | A list of related resources identified by their relative URIs | [optional] 
**uri** | **str, none_type** | The URI of the Country resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



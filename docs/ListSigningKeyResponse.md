# ListSigningKeyResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**end** | **int** |  | [optional] 
**first_page_uri** | **str** |  | [optional] 
**next_page_uri** | **str** |  | [optional] 
**page** | **int** |  | [optional] 
**page_size** | **int** |  | [optional] 
**previous_page_uri** | **str** |  | [optional] 
**signing_keys** | [**[ApiV2010AccountSigningKey]**](ApiV2010AccountSigningKey.md) |  | [optional] 
**start** | **int** |  | [optional] 
**uri** | **str** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# ApiV2010AccountCallSiprec


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created this resource | [optional] 
**call_sid** | **str, none_type** | The SID of the Call the resource is associated with | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that this resource was last updated | [optional] 
**name** | **str, none_type** | The name of this resource | [optional] 
**sid** | **str, none_type** | The SID of the Siprec resource. | [optional] 
**status** | **str, none_type** | The status - one of &#x60;stopped&#x60;, &#x60;in-progress&#x60; | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# ApiV2010AccountAvailablePhoneNumberCountryAvailablePhoneNumberNational


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address_requirements** | **str, none_type** | The type of Address resource the phone number requires | [optional] 
**beta** | **bool, none_type** | Whether the phone number is new to the Twilio platform | [optional] 
**capabilities** | [**ApiV2010AccountAvailablePhoneNumberCountryAvailablePhoneNumberLocalCapabilities**](ApiV2010AccountAvailablePhoneNumberCountryAvailablePhoneNumberLocalCapabilities.md) |  | [optional] 
**friendly_name** | **str, none_type** | A formatted version of the phone number | [optional] 
**iso_country** | **str, none_type** | The ISO country code of this phone number | [optional] 
**lata** | **str, none_type** | The LATA of this phone number | [optional] 
**latitude** | **float, none_type** | The latitude of this phone number&#39;s location | [optional] 
**locality** | **str, none_type** | The locality or city of this phone number&#39;s location | [optional] 
**longitude** | **float, none_type** | The longitude of this phone number&#39;s location | [optional] 
**phone_number** | **str, none_type** | The phone number in E.164 format | [optional] 
**postal_code** | **str, none_type** | The postal or ZIP code of this phone number&#39;s location | [optional] 
**rate_center** | **str, none_type** | The rate center of this phone number | [optional] 
**region** | **str, none_type** | The two-letter state or province abbreviation of this phone number&#39;s location | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



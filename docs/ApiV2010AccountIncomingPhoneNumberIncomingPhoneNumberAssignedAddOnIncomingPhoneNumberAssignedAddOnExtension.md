# ApiV2010AccountIncomingPhoneNumberIncomingPhoneNumberAssignedAddOnIncomingPhoneNumberAssignedAddOnExtension


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created the resource | [optional] 
**assigned_add_on_sid** | **str, none_type** | The SID that uniquely identifies the assigned Add-on installation | [optional] 
**enabled** | **bool, none_type** | Whether the Extension will be invoked | [optional] 
**friendly_name** | **str, none_type** | The string that you assigned to describe the resource | [optional] 
**product_name** | **str, none_type** | A string that you assigned to describe the Product this Extension is used within | [optional] 
**resource_sid** | **str, none_type** | The SID of the Phone Number to which the Add-on is assigned | [optional] 
**sid** | **str, none_type** | The unique string that identifies the resource | [optional] 
**unique_name** | **str, none_type** | An application-defined string that uniquely identifies the resource | [optional] 
**uri** | **str, none_type** | The URI of the resource, relative to &#x60;https://api.twilio.com&#x60; | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



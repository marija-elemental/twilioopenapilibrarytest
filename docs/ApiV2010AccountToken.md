# ApiV2010AccountToken


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str, none_type** | The SID of the Account that created the resource | [optional] 
**date_created** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was created | [optional] 
**date_updated** | **str, none_type** | The RFC 2822 date and time in GMT that the resource was last updated | [optional] 
**ice_servers** | [**[ApiV2010AccountTokenIceServers], none_type**](ApiV2010AccountTokenIceServers.md) | An array representing the ephemeral credentials | [optional] 
**password** | **str, none_type** | The temporary password used for authenticating | [optional] 
**ttl** | **str, none_type** | The duration in seconds the credentials are valid | [optional] 
**username** | **str, none_type** | The temporary username that uniquely identifies a Token | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


